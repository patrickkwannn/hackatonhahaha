package Hackaton.Service;

import Hackaton.Model.TransactionModel;
import Hackaton.Repository.DTO.TransactionRequest;

import java.util.List;

public interface TransactionServiceInterface {

    TransactionModel createTransaction(TransactionRequest transactionRequest);

    void updateTransaction(TransactionModel transactionModel);

    void deleteTransaction(String id);

    List<TransactionModel> findAll();

    TransactionModel findTransactionById(String id);

    List<TransactionModel> findByUsername(String userId);

    List<TransactionModel> findByTopic(String topic);
}
