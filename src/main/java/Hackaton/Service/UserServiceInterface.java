package Hackaton.Service;

import Hackaton.Model.UserModel;

import java.util.List;

public interface UserServiceInterface {
    void createUser(UserModel user);
    void updateUser(UserModel user);
    void deleteUser(String username);

    Boolean existsById(String username);

    List<UserModel> findAll();

    UserModel findById(String id);
}
