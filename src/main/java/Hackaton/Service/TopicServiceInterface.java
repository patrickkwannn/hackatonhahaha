package Hackaton.Service;

import Hackaton.Model.TopicModel;

import java.util.List;

public interface TopicServiceInterface {

    void createTopic(TopicModel topic);
    void updateTopic(TopicModel topic);
    void deleteTopic(String id);

    List<TopicModel> findAll();

    TopicModel findByID(String id);
}
