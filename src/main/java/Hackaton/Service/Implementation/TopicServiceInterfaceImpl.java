package Hackaton.Service.Implementation;

import Hackaton.Model.TopicModel;
import Hackaton.Repository.TopicRepository;
import Hackaton.Service.TopicServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicServiceInterfaceImpl implements TopicServiceInterface {

    @Autowired
    private TopicRepository topicRepository;

    @Override
    public void createTopic(TopicModel topic) {
        topicRepository.save(topic);
    }

    @Override
    public void updateTopic(TopicModel topic) {
        topicRepository.save(topic);
    }

    @Override
    public void deleteTopic(String id) {
        topicRepository.delete(id);
    }

    @Override
    public List<TopicModel> findAll() {
        return topicRepository.findAll();
    }

    @Override
    public TopicModel findByID(String id) {
        return topicRepository.findByTopicID(id);
    }
}