package Hackaton.Service.Implementation;

import Hackaton.Model.UserModel;
import Hackaton.Repository.UserRepository;
import Hackaton.Service.UserServiceInterface;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceInterfaceImpl implements UserServiceInterface {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void createUser(UserModel user) {
        userRepository.save(user);
    }

    @Override
    public void updateUser(UserModel user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUser(String username) {
        userRepository.delete(username);
    }

    @Override
    public Boolean existsById(String username) {
        return userRepository.exists(username);
    }

    @Override
    public List<UserModel> findAll() {
        return userRepository.findAll();
    }

    @Override
    public UserModel findById(String id) {
        return userRepository.findById(id);
    }
}
