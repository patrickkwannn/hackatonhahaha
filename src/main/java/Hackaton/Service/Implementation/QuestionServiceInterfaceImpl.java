package Hackaton.Service.Implementation;

import Hackaton.Model.QuestionModel;
import Hackaton.Model.TopicModel;
import Hackaton.Repository.DTO.QuestionRequest;
import Hackaton.Repository.QuestionRepository;
import Hackaton.Repository.TopicRepository;
import Hackaton.Service.QuestionServiceInterface;
import org.aspectj.weaver.patterns.TypePatternQuestions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceInterfaceImpl implements QuestionServiceInterface {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private TopicRepository topicRepository;

    @Override
    public QuestionModel createQuestion(QuestionRequest questionRequest) {

        TopicModel topicModel = topicRepository.getOne(questionRequest.getTopicId());
        QuestionModel newQuestion = new QuestionModel();
        newQuestion.setQuestion(questionRequest.getQuestion());
        newQuestion.setAnswer(questionRequest.getAnswer());

        newQuestion.setTopicModel(topicModel);

        questionRepository.save(newQuestion);
        return newQuestion;
    }

    @Override
    public void updateQuestion(QuestionModel question) {
        questionRepository.save(question);
    }

    @Override
    public void deleteQuestion(String id) {
        questionRepository.delete(id);
    }

    @Override
    public List<QuestionModel> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public List<QuestionModel> findByQuestion(String word) {
        return questionRepository.findByQuestion(word);
    }

    @Override
    public List<QuestionModel> findQuestionByTopic(String topic) {
        return questionRepository.findQuestionByTopic(topic);
    }


}
