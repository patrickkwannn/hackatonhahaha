package Hackaton.Service.Implementation;

import Hackaton.Model.QuestionModel;
import Hackaton.Model.TransactionModel;
import Hackaton.Model.UserModel;
import Hackaton.Repository.DTO.TransactionRequest;
import Hackaton.Repository.QuestionRepository;
import Hackaton.Repository.TransactionRepository;
import Hackaton.Repository.UserRepository;
import Hackaton.Service.TransactionServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceInterfaceImpl implements TransactionServiceInterface {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public TransactionModel createTransaction(TransactionRequest transactionRequest) {

        UserModel userr = userRepository.getOne(transactionRequest.getUserID());
        QuestionModel questionn = questionRepository.getOne(transactionRequest.getQuestionID());

        TransactionModel newTransaction = new TransactionModel();
        newTransaction.setQuestionModelT(questionn);
        newTransaction.setUserModelT(userr);

        transactionRepository.save(newTransaction);
        return newTransaction;
    }

    @Override
    public void updateTransaction(TransactionModel transactionModel) {
        transactionRepository.save(transactionModel);
    }

    @Override
    public void deleteTransaction(String id) {
        transactionRepository.delete(id);
    }

    @Override
    public List<TransactionModel> findAll() {
        return transactionRepository.findAll();
    }

    @Override
    public TransactionModel findTransactionById(String id) {
        return transactionRepository.findByTransactionID(id);
    }

    @Override
    public List<TransactionModel> findByUsername(String userId) {
        return transactionRepository.findByUsername(userId);

    }

    @Override
    public List<TransactionModel> findByTopic(String topic) {
        return transactionRepository.findByTopic(topic);
    }
}
