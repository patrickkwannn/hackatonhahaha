package Hackaton.Service;

import Hackaton.Model.QuestionModel;
import Hackaton.Model.TopicModel;
import Hackaton.Repository.DTO.QuestionRequest;

import java.util.List;

public interface QuestionServiceInterface {

    QuestionModel createQuestion(QuestionRequest questionRequest);
    void updateQuestion(QuestionModel question);
    void deleteQuestion(String id);

    List<QuestionModel> findAll();

    List<QuestionModel> findByQuestion(String word);

    List<QuestionModel> findQuestionByTopic(String topic);
}
