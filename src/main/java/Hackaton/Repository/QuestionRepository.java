package Hackaton.Repository;

import Hackaton.Model.QuestionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface QuestionRepository extends JpaRepository<QuestionModel, String> {

    @Query(value = "SELECT * FROM QUESTIONS WHERE QUESTION LIKE %:word%", nativeQuery = true)
    List<QuestionModel> findByQuestion(@Param("word") String word);

    @Query(value = "SELECT * FROM QUESTIONS q" +
            " JOIN TOPICS tp ON q.TOPIC_ID = tp.TOPIC_ID " +
            "WHERE tp.TOPIC =:topic", nativeQuery = true)
    List<QuestionModel> findQuestionByTopic(@Param("topic") String topic);

}
