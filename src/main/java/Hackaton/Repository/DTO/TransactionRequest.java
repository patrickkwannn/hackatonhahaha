package Hackaton.Repository.DTO;

public class TransactionRequest {

    private String userID;
    private String questionID;
    private String username;

    public String getUserID() {
        return userID;
    }

    public String getQuestionID() {
        return questionID;
    }

    public String getUsername() {
        return username;
    }

    public void setUserID() {
        this.userID = userID;
    }

    public void setQuestionID() {
        this.questionID = questionID;
    }

    public void setUsername() {
        this.username = username;
    }
}
