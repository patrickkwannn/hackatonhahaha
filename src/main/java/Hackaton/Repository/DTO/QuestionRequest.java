package Hackaton.Repository.DTO;

public class QuestionRequest {

    private String question;
    private String topicId;
    private String answer;

    public String getQuestion() {
        return question;
    }

    public String getTopicId() {
        return topicId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }
}
