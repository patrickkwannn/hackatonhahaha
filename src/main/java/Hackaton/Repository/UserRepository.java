package Hackaton.Repository;

import Hackaton.Model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<UserModel, String> {

    @Query(value = "SELECT * FROM USER WHERE USERNAME =:id", nativeQuery = true)
    UserModel findById(@Param("id") String id);

}
