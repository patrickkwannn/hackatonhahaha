package Hackaton.Repository;

import Hackaton.Model.TopicModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TopicRepository extends JpaRepository<TopicModel, String> {

    @Query(value = "SELECT * FROM TOPICS WHERE TOPIC_ID =:id", nativeQuery = true)
    TopicModel findByTopicID(@Param("id") String id);

}
