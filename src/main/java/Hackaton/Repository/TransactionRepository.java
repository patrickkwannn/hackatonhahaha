package Hackaton.Repository;

import Hackaton.Model.TransactionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransactionRepository extends JpaRepository<TransactionModel, String> {

    @Query(value = "SELECT * FROM TRANSACTION WHERE TRANSACTION_ID =:id", nativeQuery = true)
    TransactionModel findByTransactionID(@Param("id") String id);

    @Query(value = "SELECT * FROM TRANSACTION WHERE USERNAME =:username", nativeQuery = true)
    List<TransactionModel> findByUsername(@Param("username") String username);

    @Query(value = "SELECT * FROM TRANSACTION t " +
            "JOIN QUESTIONS q ON t.QUESTION_ID = q.QUESTION_ID " +
            "JOIN TOPICS tp ON q.TOPIC_ID = tp.TOPIC_ID " +
            "WHERE tp.TOPIC =:topic", nativeQuery = true)
    List<TransactionModel> findByTopic(@Param("topic") String topic);

}
