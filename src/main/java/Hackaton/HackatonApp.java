package Hackaton;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackatonApp {
    public static void main(String[] args){
        SpringApplication.run(HackatonApp.class, args);
    }
}
