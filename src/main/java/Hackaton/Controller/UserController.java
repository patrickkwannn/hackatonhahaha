package Hackaton.Controller;

import Hackaton.Model.UserModel;
import Hackaton.Service.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    private UserServiceInterface userServiceInterface;

    @CrossOrigin
    @PostMapping(value = "/saveuser")
    public ResponseEntity<?> saveUser(@Valid @RequestBody UserModel user){

        //{
        //  "username":"",
        //  "email":""
        //}

        UserModel newUser = new UserModel();
        newUser.setUsername("username " + user.getUsername() + " has been added");
        newUser.setEmail("email " + user.getEmail() + " has been added");

        userServiceInterface.createUser(user);

        return new ResponseEntity(newUser, HttpStatus.OK);

    }

    @CrossOrigin
    @PutMapping(value = "/updateuser")
    public ResponseEntity<?> updateUser(@RequestBody UserModel user) {

        userServiceInterface.updateUser(user);

        return new ResponseEntity(user, HttpStatus.OK);

    }

    @CrossOrigin
    @GetMapping(value = "/deleteuser/{username}")
    public ResponseEntity<?> deleteuser(@PathVariable String username){

        userServiceInterface.deleteUser(username);

        return new ResponseEntity(username, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/getall")
    public ResponseEntity<?> getall() {
        List<UserModel> users = userServiceInterface.findAll();

        return new ResponseEntity(users, HttpStatus.OK);
    }

}
