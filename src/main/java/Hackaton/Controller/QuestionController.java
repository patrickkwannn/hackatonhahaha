package Hackaton.Controller;


import Hackaton.Model.QuestionModel;
import Hackaton.Repository.DTO.QuestionRequest;
import Hackaton.Service.QuestionServiceInterface;

import Hackaton.Service.TopicServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/question")
public class QuestionController {

    @Autowired
    private QuestionServiceInterface questionServiceInterface;

    @Autowired
    private TopicServiceInterface topicServiceInterface;

    @CrossOrigin
    @PostMapping(value = "/addquestion")
    public ResponseEntity<?> addQuestion(@Valid @RequestBody QuestionRequest questionRequest) {

        //{
        //  "topicId" : "",
        //  "question" : "",
        //  "answer" : ""
        //}{



        QuestionModel question = questionServiceInterface.createQuestion(questionRequest);

        return new ResponseEntity(question, HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping(value = "/updatequestion")
    public ResponseEntity<?> updateQuestion(@RequestBody QuestionModel question){

        questionServiceInterface.updateQuestion(question);

        return new ResponseEntity(question, HttpStatus.OK);

    }

    @CrossOrigin
    @GetMapping(value = "/deletequestion/{id}")
    public ResponseEntity<?> deleteQuestion(@PathVariable String id){

        questionServiceInterface.deleteQuestion(id);

        return new ResponseEntity(id, HttpStatus.OK);

    }

    @CrossOrigin
    @GetMapping(value = "/viewall")
    public ResponseEntity<?> viewAll() {
        List<QuestionModel> question1 = questionServiceInterface.findAll();

        return new ResponseEntity(question1, HttpStatus.OK);

    }

    @CrossOrigin
    @GetMapping(value = "/searchbyquestion/{word}")
    public ResponseEntity<?> searchQuestion(@PathVariable String word) {
        List<QuestionModel> questions = questionServiceInterface.findByQuestion(word);

        return new ResponseEntity(questions, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/searchquestionbytopic/{topic}")
    public ResponseEntity<?> searchByTopic(@PathVariable String topic) {
        List<QuestionModel> questionModels = questionServiceInterface.findQuestionByTopic(topic);

        return new ResponseEntity(questionModels, HttpStatus.OK);
    }

}
