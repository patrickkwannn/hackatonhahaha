package Hackaton.Controller;


import Hackaton.Model.TopicModel;
import Hackaton.Service.TopicServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/topic")
public class TopicController {

    @Autowired
    private TopicServiceInterface topicServiceInterface;

    @CrossOrigin
    @PostMapping(value = "/savetopic")
    public ResponseEntity<?> saveTopic(@RequestBody TopicModel topic){

        //{
        // "topic":""
        //}

        topicServiceInterface.createTopic(topic);

        return new ResponseEntity(topic, HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping(value = "/updatetopic")
    public ResponseEntity<?> updateTopic(@RequestBody TopicModel topic){

        topicServiceInterface.updateTopic(topic);

        return new ResponseEntity(topic, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/deletetopic/{id}")
    public ResponseEntity<?> deleteTopic(@PathVariable String id){

        topicServiceInterface.deleteTopic(id);

        return new ResponseEntity(id, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/getalltopics")
    public ResponseEntity<?> getAll() {
        List<TopicModel> topics = topicServiceInterface.findAll();

        return new ResponseEntity(topics, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/getbyid/{id}")
    public ResponseEntity<?> getById(@PathVariable String id) {
        TopicModel thistopic = topicServiceInterface.findByID(id);

        return new ResponseEntity(thistopic, HttpStatus.OK);

    }

}
