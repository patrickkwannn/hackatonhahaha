package Hackaton.Controller;


import Hackaton.Model.TransactionModel;
import Hackaton.Repository.DTO.TransactionRequest;
import Hackaton.Service.TransactionServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/transaction")
public class TransactionController {

    @Autowired
    private TransactionServiceInterface transactionServiceInterface;

    @CrossOrigin
    @PostMapping(value = "/savetransaction")
    public ResponseEntity<?> createTransaction(@RequestBody TransactionRequest transactionRequest) {

        //{
        //  "questionID":"",
        //  "userID":""
        //}

        TransactionModel transaction = transactionServiceInterface.createTransaction(transactionRequest);

        return new ResponseEntity(transaction, HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping(value = "/updatetransaction")
    public ResponseEntity<?> updateTransaction(@RequestBody TransactionModel transactionModel) {
        transactionServiceInterface.updateTransaction(transactionModel);

        return new ResponseEntity(transactionModel, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/getalltransaction")
    public ResponseEntity<?> getAllTransaction() {
        List<TransactionModel> transaction1 = transactionServiceInterface.findAll();
        return new ResponseEntity(transaction1, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/gettransactionbyid/{id}")
    public ResponseEntity<?> getTransactionById(@PathVariable String id) {
        TransactionModel thisTransaction = transactionServiceInterface.findTransactionById(id);

        return new ResponseEntity(thisTransaction, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/gettransactionbyusername/{id}")
    public ResponseEntity<?> getTransactionByUsername(@PathVariable String id) {
        List<TransactionModel> transaction = transactionServiceInterface.findByUsername(id);

        return new ResponseEntity(transaction, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/gettransactionbytopic/{topic}")
    public ResponseEntity<?> getTrasnactionByTopic(@PathVariable String topic) {
        List<TransactionModel> transaction = transactionServiceInterface.findByTopic(topic);

        return new ResponseEntity(transaction, HttpStatus.OK);
    }

}
