package Hackaton.Model;

import Hackaton.IDGenerator.StringPrefixedSequenceIdGenerator;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TRANSACTION")
@DynamicUpdate
public class TransactionModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "TRANSACTION_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_seq")
    @GenericGenerator(
            name = "book_seq",
            strategy = "Hackaton.IDGenerator.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "50"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "TRANS"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d")})
    private String transactionID;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "USERNAME")
    private UserModel userModelT;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "QUESTION_ID")
    private QuestionModel questionModelT;

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public UserModel getUserModelT() {
        return userModelT;
    }

    public void setUserModelT(UserModel userModelT) {
        this.userModelT = userModelT;
    }

    public QuestionModel getQuestionModelT() {
        return questionModelT;
    }

    public void setQuestionModelT(QuestionModel questionModelT) {
        this.questionModelT = questionModelT;
    }
}
